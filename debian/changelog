python-django-braces (1.16.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.16.0
  * Remove python3-mock from build dependencies
  * Remove Django4 patch applied upstream

 -- Alexandre Detiste <tchet@debian.org>  Tue, 17 Dec 2024 22:31:28 +0100

python-django-braces (1.15.0-4) unstable; urgency=medium

  * Team upload.
  * Remove python3-six from build dependencies
  * Use new dh-sequence-python3

 -- Alexandre Detiste <tchet@debian.org>  Tue, 27 Aug 2024 20:52:13 +0200

python-django-braces (1.15.0-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + python-django-braces-doc: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 11:22:58 +0100

python-django-braces (1.15.0-2) unstable; urgency=medium

  * Add patch for Django 4 compatibility (Closes: #1013484).
  * Bump Standards-Version to 4.6.1.0.
  * Update year in d/copyright.
  * Depend on python3-all for autopkgtests.

 -- Michael Fladischer <fladi@debian.org>  Thu, 28 Jul 2022 19:38:51 +0000

python-django-braces (1.15.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Michael Fladischer ]
  * New upstream release.
  * Refresh patches.
  * Bump debhelper version to 13.
  * Bump Standards-Version to 4.6.0.1.
  * Add python3-factory-boy, python3-mock, python3-pytest, python3-
    pytest-cov and python3-pytest-django to Build-Depends.
  * Run upstream tests during build.
  * Use uscan version 4.
  * Enable upstream testsuite for autopkgtests.
  * Remove unnecessary autopkgtest-pkg-python testsuite.

 -- Michael Fladischer <fladi@debian.org>  Sun, 07 Nov 2021 21:18:12 +0000

python-django-braces (1.14.0-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches.

 -- Michael Fladischer <fladi@debian.org>  Tue, 14 Jan 2020 11:27:56 +0100

python-django-braces (1.13.0-2) unstable; urgency=medium

  * Team upload
  * Override module name to import (Closes: #946732)

 -- Emmanuel Arias <emmanuelarias30@gmail.com>  Sun, 15 Dec 2019 00:29:31 -0300

python-django-braces (1.13.0-1) unstable; urgency=low

  * New upstream release.
  * Add myself to uploaders.
  * Update d/watch to use pypi, as upstream github repo no longer
    provides release tags.
  * Update patch to include the complete missing upstream sphinx
    configuration.
  * Bump debhelper version to 12.
  * Bump Standards-Version to 4.4.1.
  * Run wrap-and-sort -bast to reduce diff size of future changes.
  * Add patch to temporarily disable releases sphinx extension.
  * Set Rules-Requires-Root: no.
  * Enable autopkgtest-pkg-python testsuite.

 -- Michael Fladischer <fladi@debian.org>  Fri, 22 Nov 2019 12:30:00 +0100

python-django-braces (1.9.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use 'python3 -m sphinx' instead of sphinx-build for building docs
  * Use debhelper-compat instead of debian/compat.

  [ Michael Fladischer ]
  * Add debian/gbp.conf.

  [ Thomas Goirand ]
  * Team upload.
  * Removed Python 2 support.

 -- Thomas Goirand <zigo@debian.org>  Fri, 26 Jul 2019 18:08:26 +0200

python-django-braces (1.9.0-1) unstable; urgency=low

  [ Michael Fladischer ]
  * Team upload.
  * Use python3-sphinx to build documentation.
  * Clean up files to allow two build in a row.
  * New upstream release.
  * Bump Standards-Version to 3.9.8.
  * Shorten the descriptions for all packages.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

 -- Michael Fladischer <fladi@debian.org>  Sun, 19 Jun 2016 12:01:30 +0200

python-django-braces (1.8.0-2) unstable; urgency=medium

  [ Michael Fladischer ]
  * Team upload.
  * Add python(3)-six to Build-Depends (Closes: #802135).
  * Disable tests completely as they depend on pytest-django which is no yet in the archive and they fail with python3.5.

  [ SVN-Git Migration ]
  * git-dpm config
  * Update Vcs fields for git migration

 -- Michael Fladischer <fladi@debian.org>  Mon, 19 Oct 2015 15:15:33 +0200

python-django-braces (1.8.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Add Python3 support through a separate package (Closes: #763076).
  * Move documentation to a separate package.
  * Bump Standards-Version to 3.9.6.
  * Add alabaster-path.patch to detect theme path for sphinx.
  * Update years in d/copyright.
  * Rename BSD-3-clause to BSD-3-clause-Brack3t in d/copyright.
  * Format d/copyright to fully conform to DEP5.
  * Directly call sphinx-build in d/rules instead of using the upstream
    Makefile.
  * Add override in d/rules to disable compression of changelog.html.
  * Add python-all and python3-all to Build-Depends, replacing python.
  * Add python-alabaster, python3-setuptools and python3-django to Build-
    Depends-Indep.
  * Add python-django-braces-doc to Suggests for both python-django-
    braces and python3-django-braces.
  * Install upstream changelog directly from source.

 -- Michael Fladischer <fladi@debian.org>  Fri, 19 Jun 2015 22:38:22 +0200

python-django-braces (1.4.0-1) unstable; urgency=medium

  * New upstream version
  * debian/control:
    - add dh-python and python-releases | python3-releases to
      Build-Depends-Indep
  * debian/rules:
    - install new upstream changelog
    - switch to pybuild
    - fix typo s/override_dh_auth_clean/override_dh_auto_clean/
  * tested with Django 1.7 (Closes: #755636)

 -- Jan Dittberner <jandd@debian.org>  Sat, 27 Sep 2014 18:58:56 +0200

python-django-braces (1.3.1-1) unstable; urgency=medium

  * New upstream version
  * bump Standards-Version to 3.9.5 (no changes)
  * use wrap-and-sort

 -- Jan Dittberner <jandd@debian.org>  Sat, 01 Feb 2014 22:00:10 +0100

python-django-braces (1.2.2-1) unstable; urgency=low

  * Initial release (Closes: #725624)

 -- Jan Dittberner <jandd@debian.org>  Sat, 12 Oct 2013 18:31:03 +0200
